import Battle from "../../domain/Battle";
import BattleRepository from "../../domain/Battle.repository";
import BattleSchema from "../../domain/Battle.schema";

export default class MongoDB implements BattleRepository {
  save = async (): Promise<any | Error> => {
    return await BattleSchema.find()
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err;
      });
  };
}
