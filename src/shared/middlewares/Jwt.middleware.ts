import * as jwt from 'jsonwebtoken'
export function JWTMiddleware(req, res, next) {

    let token = req.get('Authorization');

    token = token.split(' ');
    if (token.length !== 2) {
        return res.status(401).json({
            mensaje: 'No autorizado'
        });
    }

    if (token[0] !== 'Bearer') {
        return res.status(401).json({
            mensaje: 'No autorizado'
        });
    }

    return jwt.verify(token[1], 'Passphrase', (err, decoded) => {

        if (err) {
            return res.status(401).json({
                mensaje: 'No autorizado',
                err
            });
        }
        req.usuario = decoded.usuario;
        //req.locals.usuario = decoded.usuario;
        next();

        //     })

    }
    )
}
