import Movement from "./Movement";

interface MovementRepository {
  findById(id: string): Promise<Movement>;

  findAll(): Promise<Movement[]>;

  create(movement: Movement): Promise<void | Error>;

  delete(id: string): Promise<void | Error>;

  update(movement: Movement): Promise<Movement | Error>;
}

export default MovementRepository;
