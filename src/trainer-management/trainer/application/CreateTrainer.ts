import Trainer from "../domain/Trainer";
import TrainerRepository from "../domain/Trainer.repository";

export default class CreateTrainer {
    trainerRepository: TrainerRepository

    constructor(trainerRepository: TrainerRepository) {
        this.trainerRepository = trainerRepository
    }

    create = async (trainer: Trainer) => {
        try {
            const created = await this.trainerRepository.create(trainer)
            return created
        } catch (error) {
            throw new Error(error.message)
        }

    }
}