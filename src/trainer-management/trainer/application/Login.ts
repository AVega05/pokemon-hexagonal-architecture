import LoginRepository from '../infrastructure/repositories/Login.repository'

export default class Login {
    LoginRepository: LoginRepository;
    constructor(LoginRepository: LoginRepository) {
        this.LoginRepository = LoginRepository;
    }

    GetJWT = async () => {
        try {
            return await this.LoginRepository.JwtSign();
        } catch (error) {

        }
    }
}