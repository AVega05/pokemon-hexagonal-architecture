import CreateTrainer from "./CreateTrainer";
import DeleteTrainer from "./DeleteTrainer";
import FindTrainer from "./FindTrainer";
import UpdateTrainer from "./UpdateTrainer";
import Login from "./Login";
export {
    CreateTrainer,
    DeleteTrainer,
    FindTrainer,
    UpdateTrainer,
    Login
}