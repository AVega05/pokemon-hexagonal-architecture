import Trainer from "./Trainer";
export default interface LoginRepository {
  JwtSign(): Promise<string>;
}
