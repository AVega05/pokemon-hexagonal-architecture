import Trainer from "./Trainer";

export default interface TrainerRepository {

    create(trainer: Trainer): Promise<Object | Error>;

    findById(id: string): Promise<Trainer>;

    findByName(name: string): Promise<Trainer>;

    findAll(): Promise<Trainer[]>;

    update(trainer: Trainer): Promise<Trainer | Error>;

    delete(id: string): Promise<void | Error>;
}