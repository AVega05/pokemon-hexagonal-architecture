import Trainer from "../../domain/Trainer";
import LoginInterface from "../../domain/Login.interface";
import jwt from "jsonwebtoken";
export default class LoginRepository implements LoginInterface {
  Trainer: Trainer;
  expired: Number;
  constructor(Trainer: Trainer) {
    this.Trainer = Trainer;
    this.expired = 30 * 60 * 30;
  }

  JwtSign = async (): Promise<string> => {
    try {
      const wt = await jwt.sign({ Trainer: this.Trainer }, "Passphrase", {
        expiresIn: this.expired,
      });
      const token: string = "Bearer " + wt;
      return token;
    } catch (error) {
      throw new Error(error.message);
    }
  };
}
