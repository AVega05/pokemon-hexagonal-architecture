import Trainer from "../../domain/Trainer";
import TrainerRepository from "../../domain/Trainer.repository";
import TrainerSchema from "./Trainer.schema";


export default class MongooseRepository implements TrainerRepository {
  constructor() { }

  findAll = async (): Promise<Trainer[]> => {
    return await TrainerSchema.find()
      .then((res) => {
        return res;
      })
      .catch((err) => {
        throw new Error(err);
      });
  };

  findById = async (id: string): Promise<Trainer> => {
    return await TrainerSchema.findOne({ idTrainer: id })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        throw new Error(err);
      });
  };

  findByName = async (name: string): Promise<Trainer> => {
    return await TrainerSchema.findOne({ name: name })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        throw new Error(err);
      });
  };

  create = async (trainer: Trainer): Promise<Object | Error> => {
    const trainerJSON = trainer.toJSON();
    return await TrainerSchema.create(trainerJSON)
      .then((res) => {
        return trainerJSON;
      })
      .catch((err) => {
        throw new Error(err);
      });
  };

  update = async (Trainer: Trainer): Promise<Trainer> => {
    const trainerJSON = Trainer.toJSON();
    const id = Trainer.idTrainer;
    return await TrainerSchema.findOneAndUpdate({ idTrainer: id }, trainerJSON)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        throw new Error(err);
      });
  };

  delete = async (id: string): Promise<void | Error> => {
    return await TrainerSchema.deleteOne({ idTrainer: id })
      .then((res) => {
        return;
      })
      .catch((err) => {
        throw new Error(err);
      });
  };
}
