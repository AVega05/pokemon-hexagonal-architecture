import { Request, Response, NextFunction } from "express";
import TrainerSchema from "./repositories/Trainer.schema";
import bcrypt from "bcryptjs"
import jwt from "jsonwebtoken"
import MongooseRepository from "./repositories/Mongoose.repository";
import LoginRepository from "./repositories/Login.repository"
import {
    CreateTrainer,
    DeleteTrainer,
    FindTrainer,
    UpdateTrainer,
    Login
} from "../application";
import Trainer from "../domain/Trainer";
export default class TrainerController {

    createTrainer: CreateTrainer
    deleteTrainer: DeleteTrainer
    findTrainer: FindTrainer
    updateTrainer: UpdateTrainer
    LoginRepository: LoginRepository


    constructor() {
        const BD = new MongooseRepository();
        this.createTrainer = new CreateTrainer(BD);
        this.deleteTrainer = new DeleteTrainer(BD);
        this.findTrainer = new FindTrainer(BD);
        this.updateTrainer = new UpdateTrainer(BD);
    }


    create = async (request: Request, response: Response, next: NextFunction) => {
        try {

            const { name, gender, pokemons } = request.body;
            const trainer = new Trainer({ name, gender, pokemons })
            const trainerCrated = await this.createTrainer.create(trainer);
            const JWT = new LoginRepository(trainer);
            const login = new Login(JWT);
            return response.status(201).send(await login.GetJWT());


        } catch {

        }
    }
    // LOOOGIN FUNCTION
    login = async (request: Request, response: Response, next: NextFunction) => {
        try {
            const { name } = request.body;
            const trainerSearch = await this.findTrainer.getByName(name);
            const JWT = new LoginRepository(trainerSearch);
            const login = new Login(JWT);
            return response.status(201).send(await login.GetJWT());
        } catch {

        }
    }

    test = async (request: Request, response: Response, next: NextFunction) => {
        try {
            //return response.status(201).send(request.locals.usuario);
            return response.status(201).send();
        } catch (error) {

        }
    }
};
