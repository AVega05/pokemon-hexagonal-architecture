import express from "express";
import TrainerController from "./Trainer.controller";
import { JWTMiddleware } from "../../../shared/middlewares/Jwt.middleware";
export default class TrainerRouter {

    public path = "/trainer";
    public router = express.Router();
    public trainerController: TrainerController;

    constructor() {
        this.trainerController = new TrainerController();
        this.router.post(`${this.path}`, this.trainerController.create)
        this.router.post(`${this.path}/login`, this.trainerController.login)
        this.router.get(`${this.path}/test`, JWTMiddleware, this.trainerController.test)
    }


}