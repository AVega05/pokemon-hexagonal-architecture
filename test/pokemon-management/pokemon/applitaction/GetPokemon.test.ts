import MockPokemon from "../__mocks__/PokemonRepositoryMock"
import Pokemon from "../../../../src/pokemon-management/pokemon/domain/Pokemon"
import { FindPokemon } from "../../../../src/pokemon-management/pokemon/application"

let repository: MockPokemon;
let finder: FindPokemon;

beforeEach(() => {
    repository = new MockPokemon();
    finder = new FindPokemon(repository);
})

describe('Pokemon finder', () => {
    it('Should Obtain all Pokemons', async () => {

        await finder.getAll()
    })
})