import MockPokemon from "../__mocks__/PokemonRepositoryMock"
import Pokemon from "../../../../src/pokemon-management/pokemon/domain/Pokemon"
import { CreatePokemon } from "../../../../src/pokemon-management/pokemon/application"
import Type from "../../../../src/shared/domain/Type"

let repository: MockPokemon;
let creator: CreatePokemon;

beforeEach(() => {
    repository = new MockPokemon();
    creator = new CreatePokemon(repository);
})

describe('Pokemon Creator', () => {
    it('Should Create a Valid Pokemon', async () => {
        const name = 'Nicolasmon'
        const type = Type.FAIRY
        const attack = 999
        const defense = 1
        const health = 1

        await creator.create(name, type, attack, defense, health)
    })
})