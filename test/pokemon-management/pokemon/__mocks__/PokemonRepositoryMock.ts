import PokemonRepository from "../../../../src/pokemon-management/pokemon/domain/Pokemon.repository"
import Pokemon from "../../../../src/pokemon-management/pokemon/domain/Pokemon"

export default class PokemonRepositoryMock implements PokemonRepository {

    private mockSave = jest.fn();

    findById(id: string): Promise<Pokemon> {

        this.mockSave(id);
        return
    }
    async findAll(): Promise<Pokemon[] | Error> {

        this.mockSave();
        return

    }
    async create(pokemon: Pokemon): Promise<void | Error> {

        this.mockSave(pokemon);

    }
    delete(id: string): Promise<void | Error> {
        throw new Error("Method not implemented.")
    }
    update(id: string, pokemon: Pokemon): Promise<Pokemon | Error> {
        throw new Error("Method not implemented.")
    }

    assertLastSavedPokemonIs(expected: Pokemon): void {
        const mock = this.mockSave.mock;
        const lastSavedPokemon = mock.calls[mock.calls.length - 1][0] as Pokemon;
        expect(lastSavedPokemon).toBeInstanceOf(Pokemon);
        expect(lastSavedPokemon.name).toEqual(expected.name);
    }

}
